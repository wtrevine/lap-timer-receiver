#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <SPI.h>
#include "lora.h"
#include "counters.h"
#include "web.h"


#define STASSID       "Trevine"
#define STASSID2      "Trevine1"
#define STAPSK        "abcw03252026"
#define TIMER         5000 //x0,2uS
#define LED           2
#define RESET_LORA    5
#define EN_LORA       15
#define DIO           4
#define BUZZER        16

uint8_t data_rx[60], address = 0, data_rx2;
uint8_t read_all = 0, memory_lora[60], i, send_tx = 0;
uint8_t status = 0xff;

rx_buffer_t rx_buffer;
uint8_t tx_buffer[15];
lap_timer_t lt;
volatile counter_t counter;


ESP8266WebServer server(80);

//************************************************************************************************************
void ICACHE_RAM_ATTR onTimerISR() {
  timer1_write(TIMER);

  counters_interrupt(MILLISECONDS);
}
//************************************************************************************************************

void start_wifi(void) {
  uint8_t tentativas;

  WiFi.mode(WIFI_STA);
  WiFi.begin(STASSID, STAPSK);
  Serial.println("");
  Serial.print("Conectando em: ");
  Serial.println(STASSID);
  for (tentativas = 0; tentativas < 10; tentativas++) {
    delay(500);
    Serial.print(".");
    if (WiFi.status() == WL_CONNECTED)
      goto conectado;
  }

  WiFi.begin(STASSID2, STAPSK);
  Serial.print("Conectando em: ");
  Serial.println(STASSID2);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
conectado:
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(STASSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("laptimer")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  server.on("/laptimer", handleLapTimer);
  server.on("/sensors", handleSensorData);
  server.on("/send", handleSend);
  server.on("/config_channel", handleConfigChannel);
  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);

  //--------------------------Add versão 2
  const char * headerkeys[] = {"User-Agent", "Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys) / sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize);
  //-------------------------

  server.begin();
  Serial.println("HTTP server started");
}

void setup(void) {
  pinMode(LED, OUTPUT);
  pinMode(RESET_LORA, OUTPUT);
  pinMode(EN_LORA, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(DIO, INPUT);
  digitalWrite(LED, 1);

  Serial.begin(115200);
  SPI.begin();
  counters_init();
  lora_init();
  start_wifi();

  lt.beep = 5;

  timer1_attachInterrupt(onTimerISR);
  timer1_enable(TIM_DIV16, TIM_EDGE, TIM_SINGLE);
  timer1_write(TIMER);

  memcpy(&lt.channel[CH1].name[0], "1", 1);
  memcpy(&lt.channel[CH2].name[0], "2", 1);
  memcpy(&lt.channel[CH3].name[0], "3", 1);
  memcpy(&lt.channel[CH4].name[0], "4", 1);
}

void loop(void) {
  server.handleClient();
  MDNS.update();
  counters_overflow_proccess();

  //if (lt.status != status) {
  //  status = lt.status;
  // }

  if (lt.beep) {
    if (!counter.buzzer_on.enable && !counter.buzzer_off.enable) {
      lt.beep--;
      digitalWrite(BUZZER, 1);
      counters_reset(&counter.buzzer_on, TRUE);
      Serial.println("BEEP");
    }
  }

  switch (lt.status) {
    case LORA_IDLE:
      if (lt.transmit == true) {
        if (lt.send == TX_NULL)
          lt.send = TX_ACK;
        lt.status = LORA_SEND;
        lt.transmit = false;
      }
      else
        lt.status = LORA_RECEIVE;
      break;

    case LORA_RECEIVE:
      lora_send(0x01, 0x8D);
      lora_send(0x40, 0x00);
      lt.status = LORA_RECEIVING;
      break;

    case LORA_RECEIVING:
      if (digitalRead(DIO)) {
        lora_check_receive();
        lt.status = LORA_IDLE;
      }
      break;

    case LORA_SEND:
      lora_send(0x01, 0x89);
      lora_send(0x40, 0x40);
      if (lora_transmit(lt.send))
        lt.status = LORA_SENDING;
      else
        lt.status = LORA_IDLE;
      break;

    case LORA_SENDING:
      if (digitalRead(DIO)) {
        if ((lora_read(0x12) & 0x08) == 0)
          Serial.println("ERRO SEND");
        lora_send(18, 0xFF);
        lt.send = TX_NULL;
        if (lt.receive == true) {
          lt.status = LORA_RECEIVE;
          counters_reset(&counter.read, true);
        }
        else
          lt.status = LORA_IDLE;
      }
      break;
  }
}
