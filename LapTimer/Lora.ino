uint8_t size_tx;

uint16_t convert_16_data(uint8_t* buffer) {
  return (*buffer) | (*(buffer + 1) << 8);
}

uint32_t convert_32_data(uint8_t* buffer) {
  return (*buffer) | (*(buffer + 1) << 8) | (*(buffer + 2) << 16);
}

uint8_t lora_read(uint8_t address)
{
  uint8_t i, data_;
  digitalWrite(EN_LORA, LOW);
  SPI.transfer(address);
  data_ = SPI.transfer(0);
  digitalWrite(EN_LORA, HIGH);
  return data_;
}

void lora_read_array(uint8_t address, uint8_t* data, uint8_t size)
{
  uint8_t i;
  digitalWrite(EN_LORA, LOW);
  SPI.transfer(address);
  for (i = 0; i < size; i++) {
    data[i] = SPI.transfer(0);
  }
  digitalWrite(EN_LORA, HIGH);
}

void lora_send(uint8_t address, uint8_t data)
{
  address |= 0x80;
  digitalWrite(EN_LORA, LOW);
  SPI.transfer(address);
  SPI.transfer(data);
  digitalWrite(EN_LORA, HIGH);
}

void lora_send_array(uint8_t* data, uint8_t size)
{
  uint8_t i;
  digitalWrite(EN_LORA, LOW);
  SPI.transfer(0x80);
  for (i = 0; i < size; i++) {
    SPI.transfer(data[i]);
  }
  digitalWrite(EN_LORA, HIGH);
  lora_send(0x01, 0x8B);
}

void lora_update_variables() {
  uint8_t ch = 0xFF, i;

  if (rx_buffer.address & 0xE0) {
    if (rx_buffer.address & 0x20) {
      lt.transmit = true;
      Serial.print("Solicita resposta - ");
    }
    if (rx_buffer.address & 0x40) {
      Serial.print("Solicita Beep - ");
      lt.beep++;
    }
  }

  switch (rx_buffer.address & 0x1F) {
    case RX_CH1:
      ch = CH1;
    case RX_CH2:
      if (ch == 0xff)
        ch = CH2;
    case RX_CH3:
      if (ch == 0xff)
        ch = CH3;
    case RX_CH4:
      if (ch == 0xff)
        ch = CH4;
      Serial.print("CH ");
      Serial.println(ch);
      lt.channel[ch].lap = rx_buffer.rx.ch.lap;
      for (i = 0; i < NUMBER_LAP; i++)
        lt.channel[ch].lap_timer[i] = convert_32_data(&rx_buffer.rx.ch.laps[i].lap[0]);
      lt.request = false;
      break;

    case RX_CONF_CH1:
      ch = CH1;
    case RX_CONF_CH2:
      if (ch == 0xff)
        ch = CH2;
    case RX_CONF_CH3:
      if (ch == 0xff)
        ch = CH3;
    case RX_CONF_CH4:
      if (ch == 0xff)
        ch = CH4;
      Serial.print("CONF ");
      Serial.println(ch);
      lt.channel[ch].frequency = convert_16_data(&rx_buffer.rx.conf.frequency[0]);
      lt.channel[ch].trigger_in = rx_buffer.rx.conf.trigger_in;
      lt.channel[ch].trigger_out = rx_buffer.rx.conf.trigger_out;
      //memcpy(&lt.channel[ch].name[0], &rx_buffer.rx.conf.name[0], 10);
      lt.channel[ch].enable = rx_buffer.rx.conf.enable;
      break;

    case RX_TIME_CH1:
      ch = CH1;
    case RX_TIME_CH2:
      if (ch == 0xff)
        ch = CH2;
    case RX_TIME_CH3:
      if (ch == 0xff)
        ch = CH3;
    case RX_TIME_CH4:
      if (ch == 0xff)
        ch = CH4;
      Serial.print("TIME ");
      Serial.println(ch);
      lt.channel[ch].lap = rx_buffer.rx.timer.lap;
      lt.channel[ch].lap_timer[rx_buffer.rx.timer.lap - 1] = convert_32_data(&rx_buffer.rx.timer.lap_timer[0]);
      break;

    case RX_REC_CH1:
      ch = CH1;
    case RX_REC_CH2:
      if (ch == 0xff)
        ch = CH2;
    case RX_REC_CH3:
      if (ch == 0xff)
        ch = CH3;
    case RX_REC_CH4:
      if (ch == 0xff)
        ch = CH4;
      Serial.print("REC ");
      Serial.println(ch);
      lt.channel[ch].record = convert_32_data(&rx_buffer.rx.rec.record[0]);
      lt.channel[ch].record_3 = convert_32_data(&rx_buffer.rx.rec.record_3[0]);
      break;

    case RX_TIMES:
      Serial.println("TIMES ");
      lt.timer_lap_min = convert_32_data(&rx_buffer.rx.timers.timer_lap_min[0]);
      lt.timer_lap_max = convert_32_data(&rx_buffer.rx.timers.timer_lap_max[0]);
      lt.timer_min_reentry = convert_32_data(&rx_buffer.rx.timers.timer_min_reentry[0]);
      break;

    case RX_RSSI:
      Serial.println("RSSI ");
      lt.channel[CH1].rssi = rx_buffer.rx.rssi.ch1;
      lt.channel[CH1].rssi_max = rx_buffer.rx.rssi.ch1_max;
      lt.channel[CH2].rssi = rx_buffer.rx.rssi.ch2;
      lt.channel[CH2].rssi_max = rx_buffer.rx.rssi.ch2_max;
      lt.channel[CH3].rssi = rx_buffer.rx.rssi.ch3;
      lt.channel[CH3].rssi_max = rx_buffer.rx.rssi.ch3_max;
      lt.channel[CH4].rssi = rx_buffer.rx.rssi.ch4;
      lt.channel[CH4].rssi_max = rx_buffer.rx.rssi.ch4_max;
      break;

    case RX_ACK:
      Serial.println("ACK");
      counters_reset(&counter.read, false);
      lt.receive = false;
      break;

    default:
      Serial.println("Pacote RX Invalido ");
  }
}

void lora_receive(uint8_t valid) {
  uint8_t size, add;

  size = lora_read(0x13);
  add = lora_read(0x10);
  lora_send(0x0D, add);

  if (valid) {
    lora_read_array(0x00, (uint8_t*)&rx_buffer, size);
    lora_update_variables();
  }
  else
    Serial.println("ERRO");
}

void lora_check_receive() {
  uint8_t value;

  value = lora_read(0x12);
  Serial.print("RX - ");
  if (value & 0x40) {
    digitalWrite(LED, 0);
    counters_reset(&counter.led, TRUE);
    if (value & 0x20) {
      Serial.print("CRC - ");
      lora_receive(0);
    }
    else {
      if (value & 0x10) {
        lora_receive(1);
      }
      else {
        Serial.print("Invalid HEader - ");
        lora_receive(0);
      }
    }
    lora_send(0x12, 0xFF);
  }
  else {
    Serial.println("ERRO Recepção");
  }
}

uint8_t lora_transmit(uint8_t type) {
  uint8_t ch = 0xFF;

  Serial.print(">>TX - ");
  if (lt.retrans == false) {
    switch (type) {
      case TX_CH1:
        ch = CH1;
      case TX_CH2:
        if (ch == 0xff)
          ch = CH2;
      case TX_CH3:
        if (ch == 0xff)
          ch = CH3;
      case TX_CH4:
        if (ch == 0xff)
          ch = CH4;
        Serial.print("CONF ");
        Serial.println(ch);
        tx_buffer[0] = type;
        memcpy(&tx_buffer[1], &lt.channel[ch].frequency, 2);
        tx_buffer[3] = lt.channel[ch].trigger_in;
        tx_buffer[4] = lt.channel[ch].trigger_out;
        size_tx = 5;
        lt.receive = true;
        break;

      case TX_SYS:
        Serial.println("TIMES");
        tx_buffer[0] = type;
        tx_buffer[1] = lt.reset;
        memcpy(&tx_buffer[2], &lt.timer_lap_min, 3);
        memcpy(&tx_buffer[5], &lt.timer_lap_max, 3);
        memcpy(&tx_buffer[8], &lt.timer_min_reentry, 3);
        size_tx = 11;
        lt.reset = 0;
        lt.receive = true;
        break;

      case TX_ACK:
        Serial.println("ACK");
        tx_buffer[0] = type;
        tx_buffer[1] = lt.request;
        size_tx = 2;
        break;

      default:
        Serial.print("Erro Lora tramiste");
        return 0;
    }
    if (lt.receive == true)
      tx_buffer[0] |= 0x20;
  }
  else {
    lt.retrans = false;
    Serial.println("Retransmição ");
  }

  lora_send(0x0D, 0x80);
  lora_send(0x22, size_tx);
  lora_send_array(tx_buffer, size_tx);
  return 1;
}

void lora_init(void)
{
  digitalWrite(EN_LORA, HIGH);
  digitalWrite(RESET_LORA, 0);
  delay(100);
  digitalWrite(RESET_LORA, 1);
  delay(500);

  lora_send(0x01, 0x80);
  delay(100);
  lora_send(0x01, 0x88);
  delay(100);
  lora_send(0x01, 0x89);

  lora_send(0x09, 0xFF); //Potencia de transmissão
  lora_send(0x0B, 0x0B); //Maxima corrente
  lora_send(0x0C, 0x23); //Ajuste e corrente de LNA - Alplificador de rx

  lora_send(0x4d, 0x87); //Habilita PA "Amplificador de potencia"

  lora_send(0x1D, 0x78); // Banda em 125 kHz - 4/8 Explicit Header mode
  lora_send(0x1E, 0xa7); //SF 4096 chips/symbol - CRC enable
  lora_send(0x1F, 0xFF); //Timeout max
  
  lora_send(0x20, 0x00);
  lora_send(0x21, 0x0A);
  delay(100);
  lt.status = LORA_IDLE;
  lt.send = TX_NULL;
  lt.request = true;
}
