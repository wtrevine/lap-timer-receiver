//#ifndef COUNTERS_H
//#define  COUNTERS_H

/* Timeout 1 MILLISECONDS */
#define TIMEOUT_1S            1000
#define TIMEOUT_LED           100
#define TIMEOUT_BUZZER_ON     200
#define TIMEOUT_BUZZER_OFF    100
#define TIMEOUT_READ          2000 //300

/* Timeout 1 SECONDS */
#define TIMEOUT_ACK   20

#define TRUE    1
#define FALSE   0

typedef enum type_time {
  SECONDS = 0,
  MILLISECONDS
} type_time_t;

typedef struct {
  uint8_t enable;
  uint8_t overflow;
  uint16_t count;
  uint16_t count_max;
  uint16_t count_overflow;
} timeout_t;

typedef struct {
  timeout_t timeout_1s;
  timeout_t ack;
  timeout_t led;
  timeout_t buzzer_on;
  timeout_t buzzer_off;
  timeout_t read;
} counter_t;


void counters_reset(volatile timeout_t * time, uint8_t enable);
void counters_proccess(volatile timeout_t * sData, uint8_t ReStart);
void counters_interrupt(uint8_t type);
void counters_init();
void counters_overflow_proccess(void);

//#endif
