
/*
  Reseta temporizador
*/
void counters_reset(volatile timeout_t * time, uint8_t enable) {
  time->count = 0;
  time->enable = enable;
  time->overflow = FALSE;
}

/*
  Processa temporizador
*/
void counters_proccess(volatile timeout_t * sData, uint8_t ReStart) {
  if (sData->enable == TRUE) {
    if ((++sData->count) >= sData->count_max) {
      sData->count = 0;
      sData->overflow = TRUE;
      sData->enable = ReStart;
    }
  } else
    sData->count = 0;
}

/*
  Chama processos
*  */
void counters_interrupt(uint8_t type) {
  switch (type) {
    case SECONDS:
      counters_proccess(&counter.ack, TRUE);
      break;

    case MILLISECONDS:
      counters_proccess(&counter.timeout_1s, TRUE);
      counters_proccess(&counter.led, FALSE);
      counters_proccess(&counter.buzzer_on, FALSE);
      counters_proccess(&counter.buzzer_off, FALSE);
       counters_proccess(&counter.read, FALSE);
      break;
  }
}

/*
  Inicia temporizadores
*/
void counters_init() {
  counter.timeout_1s.count_max = TIMEOUT_1S;
  counter.ack.count_max = TIMEOUT_ACK;
  counter.led.count_max = TIMEOUT_LED;
  counter.buzzer_on.count_max = TIMEOUT_BUZZER_ON;
  counter.buzzer_off.count_max = TIMEOUT_BUZZER_OFF;
  counter.read.count_max = TIMEOUT_READ;
  
  counters_reset(&counter.timeout_1s, TRUE);
  counters_reset(&counter.ack, TRUE);
}

/*
  Verifica estoutoros dos temporizadores
*/
void counters_overflow_proccess(void) {
  uint8_t i;

  if (counter.timeout_1s.overflow == TRUE) {
    counter.timeout_1s.overflow = FALSE;
    counters_interrupt(SECONDS);
  }

  if (counter.ack.overflow == TRUE) {
    counter.ack.overflow = FALSE;
    //if (lt.send == TX_NULL)
    //lt.send = TX_ACK;
  }

  if (counter.led.overflow == TRUE) {
    counter.led.overflow = FALSE;
    digitalWrite(LED, 1);
  }

  if (counter.buzzer_on.overflow == TRUE) {
    counter.buzzer_on.overflow = FALSE;
    digitalWrite(BUZZER, 0);
    counters_reset(&counter.buzzer_off, TRUE);
  }

  if (counter.buzzer_off.overflow == TRUE) {
    counter.buzzer_off.overflow = FALSE;
  }

  if (counter.read.overflow == TRUE) {
    counter.read.overflow = FALSE;
    lt.status = LORA_SEND;
    lt.retrans = true;
    lora_send(0x01, 0x88);
    lora_send(0x12, 0xFF);
  }


}
