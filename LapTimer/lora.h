
#define NUMBER_LAP      10
#define NUMBER_CH       4

enum {
  CH1 = 0,
  CH2,
  CH3,
  CH4
};

enum {
  RX_NULL = 0,
  RX_CH1,
  RX_CH2,
  RX_CH3,
  RX_CH4,
  RX_CONF_CH1,
  RX_CONF_CH2,
  RX_CONF_CH3,
  RX_CONF_CH4,
  RX_TIME_CH1,
  RX_TIME_CH2,
  RX_TIME_CH3,
  RX_TIME_CH4,
  RX_REC_CH1,
  RX_REC_CH2,
  RX_REC_CH3,
  RX_REC_CH4,
  RX_TIMES,
  RX_RSSI,
  RX_ACK
};

typedef enum {
  TX_NULL = 0,
  TX_CH1,
  TX_CH2,
  TX_CH3,
  TX_CH4,
  TX_SYS,
  TX_ACK
} lora_send_t;

typedef enum {
  LORA_IDLE = 0,
  LORA_RECEIVE,
  LORA_RECEIVING,
  LORA_SEND,
  LORA_SENDING
} lora_status_t;

typedef struct {
  uint8_t trigger_in;
  uint8_t trigger_out;
  uint8_t count_lap;
  uint8_t lap;
  uint8_t flag_out_zone;
  uint8_t flag_min_lap;
  uint8_t flag_min_reentry;
  uint32_t lap_timer[NUMBER_LAP];
  uint32_t record;
  uint32_t record_3;
  uint32_t timer;
  uint16_t frequency;
  uint32_t rssi_max;
  uint32_t rssi;
  uint8_t name[10];
  uint8_t enable;
} timer1_t;

typedef struct {
  timer1_t channel[NUMBER_CH];
  uint32_t timer_lap_min;
  uint32_t timer_lap_max;
  uint32_t timer_min_reentry;
  uint32_t timer;
  lora_status_t status;
  lora_send_t send;
  uint8_t reset;
  uint8_t transmit;
  uint8_t beep;
  uint8_t request;
  uint8_t retrans;
  uint8_t receive;
} lap_timer_t;

typedef struct {
  uint8_t lap[3];
} laps_t;

typedef struct {
  uint8_t lap;
  laps_t laps[NUMBER_LAP];
} rx_ch_t;

typedef struct {
  uint8_t frequency[2];
  uint8_t trigger_in;
  uint8_t trigger_out;
  uint8_t name[10];
  uint8_t enable;
} rx_conf_ch_t;

typedef struct {
  uint8_t lap;
  uint8_t lap_timer[3];
} rx_timer_ch_t;

typedef struct {
  uint8_t record[3];
  uint8_t record_3[3];
} rx_rec_ch_t;

typedef struct {
  uint8_t timer_lap_min[3];
  uint8_t timer_lap_max[3];
  uint8_t timer_min_reentry[3];
} rx_timers_t;

typedef struct {
  uint8_t ch1;
  uint8_t ch1_max;
  uint8_t ch2;
  uint8_t ch2_max;
  uint8_t ch3;
  uint8_t ch3_max;
  uint8_t ch4;
  uint8_t ch4_max;
} rx_rssi_t;

typedef union {
  rx_ch_t ch;
  rx_conf_ch_t conf;
  rx_timer_ch_t timer;
  rx_rec_ch_t rec;
  rx_timers_t timers;
  rx_rssi_t rssi;
} rx_t;

typedef struct {
  uint8_t address;
  rx_t rx;
} rx_buffer_t;

uint8_t lora_read(uint8_t address);
void lora_read_array(uint8_t address, uint8_t* data, uint8_t size);
void lora_send(uint8_t address, uint8_t data);
void lora_send_array(uint8_t* data, uint8_t size);
void lora_update_variables();
void lora_receive(uint8_t valid);
void lora_check_receive();
uint8_t lora_transmit(uint8_t type);
void lora_init(void);
