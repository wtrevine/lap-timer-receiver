
void handleLapTimer();
void handleRoot();
void handleSend();
void handleSensorData();
void handleNotFound();

const char *page = "<!DOCTYPE html>\
<html>\
<head>\
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\
<script>\
  $(function(){\
    setInterval(requestData, 500);\
    function requestData(){\
      $.get(\"/sensors\")\
        .done(function(data) {\
          console.log(data);\
          if (data) {\
            $(\"#id_hora\").text(data.hora);\
            $(\"#id_minuto\").text(data.minuto);\
            $(\"#id_segundo\").text(data.segundo);\
            $(\"#id_p1\").text(data.n1);\
            $(\"#id_p2\").text(data.n2);\
            $(\"#id_p3\").text(data.n3);\
            $(\"#id_p4\").text(data.n4);\
            $(\"#id_f1\").text(data.f1);\
            $(\"#id_f2\").text(data.f2);\
            $(\"#id_f3\").text(data.f3);\
            $(\"#id_f4\").text(data.f4);\
            $(\"#id_record1\").text(data.record1);\
            $(\"#id_record_31\").text(data.record_31);\
            $(\"#id_record2\").text(data.record2);\
            $(\"#id_record_32\").text(data.record_32);\
            $(\"#id_record3\").text(data.record3);\
            $(\"#id_record_33\").text(data.record_33);\
            $(\"#id_record4\").text(data.record4);\
            $(\"#id_record_34\").text(data.record_34);\
            $(\"#id_t_min\").attr(\"placeholder\", data.t_min);\
            $(\"#id_t_max\").attr(\"placeholder\", data.t_max);\
            $(\"#id_t_pass\").attr(\"placeholder\", data.t_pass);\
            if(document.getElementById(\"id_ch\").value == \"1\") {\
            $(\"#id_en\").attr(\"checked\", data.en1);\
            $(\"#id_freq\").attr(\"placeholder\", data.f1);\
            $(\"#id_name\").attr(\"placeholder\",data.n1);\
            $(\"#id_rssi_in\").attr(\"placeholder\", data.in1);\
            $(\"#id_rssi_out\").attr(\"placeholder\", data.out1);}\
            else if(document.getElementById(\"id_ch\").value == \"2\") {\
            $(\"#id_en\").attr(\"checked\", data.en2);\
            $(\"#id_freq\").attr(\"placeholder\", data.f2);\
            $(\"#id_name\").attr(\"placeholder\",data.n2);\
            $(\"#id_rssi_in\").attr(\"placeholder\", data.in2);\
            $(\"#id_rssi_out\").attr(\"placeholder\", data.out2);}\
            else if(document.getElementById(\"id_ch\").value == \"3\") {\
            $(\"#id_en\").attr(\"checked\", data.en3);\
            $(\"#id_freq\").attr(\"placeholder\", data.f3);\
            $(\"#id_name\").attr(\"placeholder\",data.n3);\
            $(\"#id_rssi_in\").attr(\"placeholder\", data.in3);\
            $(\"#id_rssi_out\").attr(\"placeholder\", data.out3);}\
            else if(document.getElementById(\"id_ch\").value == \"4\") {\
            $(\"#id_en\").attr(\"checked\", data.en4);\
            $(\"#id_freq\").attr(\"placeholder\", data.f4);\
            $(\"#id_name\").attr(\"placeholder\",data.n4);\
            $(\"#id_rssi_in\").attr(\"placeholder\", data.in4);\
            $(\"#id_rssi_out\").attr(\"placeholder\", data.out4);}\
            $(\"#id_lt11\").text(data.lt11);\
            $(\"#id_lt12\").text(data.lt12);\
            $(\"#id_lt13\").text(data.lt13);\
            $(\"#id_lt14\").text(data.lt14);\
            $(\"#id_lt15\").text(data.lt15);\
            $(\"#id_lt16\").text(data.lt16);\
            $(\"#id_lt17\").text(data.lt17);\
            $(\"#id_lt18\").text(data.lt18);\
            $(\"#id_lt19\").text(data.lt19);\
            $(\"#id_lt110\").text(data.lt110);\
            $(\"#id_lt21\").text(data.lt21);\
            $(\"#id_lt22\").text(data.lt22);\
            $(\"#id_lt23\").text(data.lt23);\
            $(\"#id_lt24\").text(data.lt24);\
            $(\"#id_lt25\").text(data.lt25);\
            $(\"#id_lt26\").text(data.lt26);\
            $(\"#id_lt27\").text(data.lt27);\
            $(\"#id_lt28\").text(data.lt28);\
            $(\"#id_lt29\").text(data.lt29);\
            $(\"#id_lt210\").text(data.lt210);\
            $(\"#id_lt31\").text(data.lt31);\
            $(\"#id_lt32\").text(data.lt32);\
            $(\"#id_lt33\").text(data.lt33);\
            $(\"#id_lt34\").text(data.lt34);\
            $(\"#id_lt35\").text(data.lt35);\
            $(\"#id_lt36\").text(data.lt36);\
            $(\"#id_lt37\").text(data.lt37);\
            $(\"#id_lt38\").text(data.lt38);\
            $(\"#id_lt39\").text(data.lt39);\
            $(\"#id_lt310\").text(data.lt310);\
            $(\"#id_lt41\").text(data.lt41);\
            $(\"#id_lt42\").text(data.lt42);\
            $(\"#id_lt43\").text(data.lt43);\
            $(\"#id_lt44\").text(data.lt44);\
            $(\"#id_lt45\").text(data.lt45);\
            $(\"#id_lt46\").text(data.lt46);\
            $(\"#id_lt47\").text(data.lt47);\
            $(\"#id_lt48\").text(data.lt48);\
            $(\"#id_lt49\").text(data.lt49);\
            $(\"#id_lt410\").text(data.lt410);\
            $(\"#id_ad1\").text(data.ad1);\
            $(\"#id_ad_max1\").text(data.ad_max1);\
            $(\"#id_ad2\").text(data.ad2);\
            $(\"#id_ad_max2\").text(data.ad_max2);\
            $(\"#id_ad3\").text(data.ad3);\
            $(\"#id_ad_max3\").text(data.ad_max3);\
            $(\"#id_ad4\").text(data.ad4);\
            $(\"#id_ad_max4\").text(data.ad_max4);\
            if(data.lap1 == 1 && data.lt11 != 0) $(\"#id_c11\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt11 == data.record1) $(\"#id_c11\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c11\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 2 && data.lt12 != 0) $(\"#id_c12\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt12 == data.record1) $(\"#id_c12\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c12\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 3 && data.lt13 != 0) $(\"#id_c13\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt13 == data.record1) $(\"#id_c13\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c13\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 4 && data.lt14 != 0) $(\"#id_c14\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt14 == data.record1) $(\"#id_c14\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c14\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 5 && data.lt15 != 0) $(\"#id_c15\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt15 == data.record1) $(\"#id_c15\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c15\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 6 && data.lt16 != 0) $(\"#id_c16\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt16 == data.record1) $(\"#id_c16\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c16\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 7 && data.lt17 != 0) $(\"#id_c17\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt17 == data.record1) $(\"#id_c17\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c17\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 8 && data.lt18 != 0) $(\"#id_c18\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt18 == data.record1) $(\"#id_c18\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c18\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 9 && data.lt19 != 0) $(\"#id_c19\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt19 == data.record1) $(\"#id_c19\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c19\").css(\"background-color\", \"#eee\");\
            if(data.lap1 == 10 && data.lt110 != 0) $(\"#id_c110\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt110 == data.record1) $(\"#id_c110\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c110\").css(\"background-color\", \"#eee\");\
            if(data.lap2 == 1 && data.lt21 != 0) $(\"#id_c21\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt21 == data.record2) $(\"#id_c21\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c21\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 2 && data.lt22 != 0) $(\"#id_c22\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt22 == data.record2) $(\"#id_c22\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c22\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 3 && data.lt23 != 0) $(\"#id_c23\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt23 == data.record2) $(\"#id_c23\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c23\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 4 && data.lt24 != 0) $(\"#id_c24\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt24 == data.record2) $(\"#id_c24\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c24\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 5 && data.lt25 != 0) $(\"#id_c25\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt25 == data.record2) $(\"#id_c25\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c25\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 6 && data.lt26 != 0) $(\"#id_c26\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt26 == data.record2) $(\"#id_c26\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c26\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 7 && data.lt27 != 0) $(\"#id_c27\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt27 == data.record2) $(\"#id_c27\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c27\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 8 && data.lt28 != 0) $(\"#id_c28\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt28 == data.record2) $(\"#id_c28\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c28\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 9 && data.lt29 != 0) $(\"#id_c29\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt29 == data.record2) $(\"#id_c29\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c29\").css(\"background-color\", \"#fff\");\
            if(data.lap2 == 10 && data.lt210 != 0) $(\"#id_c210\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt210 == data.record2) $(\"#id_c210\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c210\").css(\"background-color\", \"#fff\");\
            if(data.lap3 == 1 && data.lt31 != 0) $(\"#id_c31\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt31 == data.record3) $(\"#id_c31\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c31\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 2 && data.lt32 != 0) $(\"#id_c32\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt32 == data.record3) $(\"#id_c32\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c32\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 3 && data.lt33 != 0) $(\"#id_c33\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt33 == data.record3) $(\"#id_c33\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c33\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 4 && data.lt34 != 0) $(\"#id_c34\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt34 == data.record3) $(\"#id_c34\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c34\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 5 && data.lt35 != 0) $(\"#id_c35\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt35 == data.record3) $(\"#id_c35\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c35\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 6 && data.lt36 != 0) $(\"#id_c36\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt36 == data.record3) $(\"#id_c36\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c36\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 7 && data.lt37 != 0) $(\"#id_c37\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt37 == data.record3) $(\"#id_c37\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c37\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 8 && data.lt38 != 0) $(\"#id_c38\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt38 == data.record3) $(\"#id_c38\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c38\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 9 && data.lt39 != 0) $(\"#id_c39\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt39 == data.record3) $(\"#id_c39\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c39\").css(\"background-color\", \"#eee\");\
            if(data.lap3 == 10 && data.lt310 != 0) $(\"#id_c310\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt310 == data.record3) $(\"#id_c310\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c310\").css(\"background-color\", \"#eee\");\
            if(data.lap4 == 1 && data.lt41 != 0) $(\"#id_c41\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt41 == data.record4) $(\"#id_c41\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c41\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 2 && data.lt42 != 0) $(\"#id_c42\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt42 == data.record4) $(\"#id_c42\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c42\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 3 && data.lt43 != 0) $(\"#id_c43\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt43 == data.record4) $(\"#id_c43\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c43\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 4 && data.lt44 != 0) $(\"#id_c44\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt44 == data.record4) $(\"#id_c44\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c44\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 5 && data.lt45 != 0) $(\"#id_c45\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt45 == data.record4) $(\"#id_c45\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c45\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 6 && data.lt46 != 0) $(\"#id_c46\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt46 == data.record4) $(\"#id_c46\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c46\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 7 && data.lt47 != 0) $(\"#id_c47\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt47 == data.record4) $(\"#id_c47\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c47\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 8 && data.lt48 != 0) $(\"#id_c48\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt48 == data.record4) $(\"#id_c48\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c48\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 9 && data.lt49 != 0) $(\"#id_c49\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt49 == data.record4) $(\"#id_c49\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c49\").css(\"background-color\", \"#fff\");\
            if(data.lap4 == 10 && data.lt410 != 0) $(\"#id_c410\").css(\"background-color\", \"LightSalmon\");\
            else if(data.lt410 == data.record4) $(\"#id_c410\").css(\"background-color\", \"MediumSeaGreen\"); else $(\"#id_c410\").css(\"background-color\", \"#fff\");\
          }\
        }).fail(function() {\
          console.log(\"The was a problem retrieving the data.\");\
            $(\"#id_hora\").text(\"--\");\
            $(\"#id_minuto\").text(\"--\");\
            $(\"#id_segundo\").text(\"--\");\
            $(\"#id_ad\").text(\"-\");\
            $(\"#id_ad_max\").text(\"-\");\
        });\
    }\
  });\
  $(document).ready(function(){\
    $(\"#id_bt_reset\").click(function(){\
    if(document.getElementById(\"id_ch\").value == \"1\") {\
        if(confirm(\"Canal 1 sera limpo!\")){\
          $.get(\"/send\", {reset: 1});}}\
    if(document.getElementById(\"id_ch\").value == \"2\") {\
        if(confirm(\"Canal 2 sera limpo!\")){\
          $.get(\"/send\", {reset: 2});}}\
    if(document.getElementById(\"id_ch\").value == \"3\") {\
        if(confirm(\"Canal 3 sera limpo!\")){\
          $.get(\"/send\", {reset: 3});}}\
    if(document.getElementById(\"id_ch\").value == \"4\") {\
        if(confirm(\"Canal 4 sera limpo!\")){\
          $.get(\"/send\", {reset: 4});}}\
    });\
  });\
  </script>\
<style>\
table, th, td {\
  border: 1px solid black;\
  border-collapse: collapse;\
}\
th, td {\
  padding: 8px;\
  text-align: center;\
  width: 45px;\
}\
table#t01 tr:nth-child(even) {\
  background-color: #eee;\
}\
table#t01 tr:nth-child(odd) {\
 background-color: #fff;\
}\
//table#t01 th {\
 // background-color: black;\
 // color: white;\
//}\\
                   </style>\
</head>\
<body style=\"font-family: Consolas; background-color: LightGray; padding: 10px;\">\
<div style=\"line-height:0px; color:white; background-color: steelblue; margin: 10px 0; width: 850px; padding: 10px; \">\
<br><h2>Lap Timer Obelisk</h2>\
<h3 style=\"text-align: right;\"><span id=\"id_hora\"></span>:<span id=\"id_minuto\"></span>:<span id=\"id_segundo\"></span></h3>\
<br>\
</div>\
<div style=\"line-height:10px; margin: 10px 0; padding: 10px; width: 850px;\">\
<table id=\"t01\">\
  <tr>\
    <th>Piloto</th>\
    <th>Freq.</th> \
    <th>1</th> \
    <th>2</th>\
    <th>3</th> \
    <th>4</th>\
    <th>5</th>\
    <th>6</th> \
    <th>7</th>\
    <th>8</th> \
    <th>9</th>\
    <th>10</th> \
  </tr>\
  <tr>\
    <th><span id=\"id_p1\"></span></td>\
    <th><span id=\"id_f1\"></span></td>\
    <td id=\"id_c11\"><span id=\"id_lt11\"></span></td>\
    <td id=\"id_c12\"><span id=\"id_lt12\"></span></td>\
    <td id=\"id_c13\"><span id=\"id_lt13\"></span></td>\
    <td id=\"id_c14\"><span id=\"id_lt14\"></span></td>\
    <td id=\"id_c15\"><span id=\"id_lt15\"></span></td>\
    <td id=\"id_c16\"><span id=\"id_lt16\"></span></td>\
    <td id=\"id_c17\"><span id=\"id_lt17\"></span></td>\
    <td id=\"id_c18\"><span id=\"id_lt18\"></span></td>\
    <td id=\"id_c19\"><span id=\"id_lt19\"></span></td>\
    <td id=\"id_c110\"><span id=\"id_lt110\"></span></td>\
  </tr>\
    <tr>\
    <th><span id=\"id_p2\"></span></td>\
    <th><span id=\"id_f2\"></span></td>\
    <td id=\"id_c21\"><span id=\"id_lt21\"></span></td>\
    <td id=\"id_c22\"><span id=\"id_lt22\"></span></td>\
    <td id=\"id_c23\"><span id=\"id_lt23\"></span></td>\
    <td id=\"id_c24\"><span id=\"id_lt24\"></span></td>\
    <td id=\"id_c25\"><span id=\"id_lt25\"></span></td>\
    <td id=\"id_c26\"><span id=\"id_lt26\"></span></td>\
    <td id=\"id_c27\"><span id=\"id_lt27\"></span></td>\
    <td id=\"id_c28\"><span id=\"id_lt28\"></span></td>\
    <td id=\"id_c29\"><span id=\"id_lt29\"></span></td>\
    <td id=\"id_c210\"><span id=\"id_lt210\"></span></td>\
  </tr>\
      <tr>\
    <th><span id=\"id_p3\"></span></td>\
    <th><span id=\"id_f3\"></span></td>\
    <td id=\"id_c31\"><span id=\"id_lt31\"></span></td>\
    <td id=\"id_c32\"><span id=\"id_lt32\"></span></td>\
    <td id=\"id_c33\"><span id=\"id_lt33\"></span></td>\
    <td id=\"id_c34\"><span id=\"id_lt34\"></span></td>\
    <td id=\"id_c35\"><span id=\"id_lt35\"></span></td>\
    <td id=\"id_c36\"><span id=\"id_lt36\"></span></td>\
    <td id=\"id_c37\"><span id=\"id_lt37\"></span></td>\
    <td id=\"id_c38\"><span id=\"id_lt38\"></span></td>\
    <td id=\"id_c39\"><span id=\"id_lt39\"></span></td>\
    <td id=\"id_c310\"><span id=\"id_lt310\"></span></td>\
  </tr>\
      <tr>\
    <th><span id=\"id_p4\"></span></td>\
    <th><span id=\"id_f4\"></span></td>\
    <td id=\"id_c41\"><span id=\"id_lt41\"></span></td>\
    <td id=\"id_c42\"><span id=\"id_lt42\"></span></td>\
    <td id=\"id_c43\"><span id=\"id_lt43\"></span></td>\
    <td id=\"id_c44\"><span id=\"id_lt44\"></span></td>\
    <td id=\"id_c45\"><span id=\"id_lt45\"></span></td>\
    <td id=\"id_c46\"><span id=\"id_lt46\"></span></td>\
    <td id=\"id_c47\"><span id=\"id_lt47\"></span></td>\
    <td id=\"id_c48\"><span id=\"id_lt48\"></span></td>\
    <td id=\"id_c49\"><span id=\"id_lt49\"></span></td>\
    <td id=\"id_c410\"><span id=\"id_lt410\"></span></td>\
  </tr>\
</table>\
</div>\
<div style=\"display: flex; line-height:10px; margin: 10px 0; padding: 10px; width: 850px;\">\
<div style=\"float: left; width: 40%\">\
<h4>RSSI 1:&nbsp<span id=\"id_ad1\"></span>&nbsp<span id=\"id_ad_max1\"></span></h4>\
<h4>RSSI 2:&nbsp<span id=\"id_ad2\"></span>&nbsp<span id=\"id_ad_max2\"></span></h4>\
<h4>RSSI 3:&nbsp<span id=\"id_ad3\" ></span>&nbsp<span id=\"id_ad_max3\"></span></h4>\
<h4>RSSI 4:&nbsp<span id=\"id_ad4\"></span>&nbsp<span id=\"id_ad_max4\"></span></h4>\
<h4>M Volta 1:&nbsp<span id=\"id_record1\"></span>&nbsp/&nbsp<span id=\"id_record_31\"></span></h4>\
<h4>M Volta 2:&nbsp<span id=\"id_record2\"></span>&nbsp/&nbsp<span id=\"id_record_32\"></span></h4>\
<h4>M Volta 3:&nbsp<span id=\"id_record3\"></span>&nbsp/&nbsp<span id=\"id_record_33\"></span></h4>\
<h4>M Volta 4:&nbsp<span id=\"id_record4\"></span>&nbsp/&nbsp<span id=\"id_record_34\"></span></h4>\
<button id=\"id_bt_reset\">Reset</button>\
</div>\
<div style=\"float: right; width: 35%\">\
<form action='/laptimer' method='POST'><br>\
Channel:&nbsp<select id=\"id_ch\" name=\"CH\"><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option></select>&nbsp\
<input type=\"checkbox\" id=\"id_en\" name=\"EN\" value=\"1\">Enable<br><br>\
Freq:&nbsp&nbsp&nbsp&nbsp<input type='number' id=\"id_freq\" name='FREQ' min='5300' max='5999' size='10'><br><br>\
Name:&nbsp&nbsp&nbsp&nbsp<input type='text' id=\"id_name\" name='NAME' maxlength='9' size='10'><br><br>\
RSSI in: <input type='number' id=\"id_rssi_in\" name='RSSI_IN' min='1' max='100' size='10'><br><br>\
RSSI out:<input type='number' id=\"id_rssi_out\" name='RSSI_OUT' min='1' max='100' size='10'><br><br>\
<input type='submit' name='SUBMIT' value='OK'>&nbsp<input type=\"reset\" value='CANCEL'></form>\
</div>\
<div style=\"float: right; width: 25%\">\
<form action='/laptimer' method='POST'><br>\
Time min:&nbsp&nbsp<input type='number' id=\"id_t_min\" name='LAP_MIN' min='1' max='300' size='10'><br><br>\
Time max:&nbsp&nbsp<input type='number' id=\"id_t_max\" name='LAP_MAX' min='1' max='300' size='10'><br><br>\
Time pass:&nbsp<input type='number' id=\"id_t_pass\" name='T_PASS' min='1' max='300' size='10'><br><br>\
<input type='submit' name='SUBMIT' value='Configurar'>&nbsp<input type=\"reset\"></form>\
</div>\
</div>\
<div style=\"text-align: right; line-height:0px; color:white; background-color: steelblue; margin: 10px 0; width: 850px; height: 30px; padding: 10px; \">\
<h5>By W Trevine</h5>\
</div>\
</body></html>";
