

void handleLapTimer()
{
  uint8_t ch;
  if (server.hasArg("LAP_MIN") && server.hasArg("LAP_MAX") && server.hasArg("T_PASS")) {
    if (server.arg("LAP_MIN") != 0)
      lt.timer_lap_min = server.arg("LAP_MIN").toInt() * 1000;
    if (server.arg("LAP_MAX") != 0)
      lt.timer_lap_max = server.arg("LAP_MAX").toInt() * 1000;
    if (server.arg("T_PASS") != 0)
      lt.timer_min_reentry = server.arg("T_PASS").toInt() * 1000;
      
    Serial.println(server.arg("LAP_MIN").toInt());
    Serial.println(server.arg("LAP_MAX").toInt());
    Serial.println(server.arg("T_PASS").toInt());
    
    lt.send = TX_SYS;
  }

  if (server.hasArg("CH")) {
    ch = server.arg("CH").toInt() - 1;
    Serial.print("CH: ");
    Serial.println(ch);
    if (server.arg("EN") != 0)
      lt.channel[ch].enable = 1;
    else
      lt.channel[ch].enable = 0;
    Serial.print("EN: ");
    Serial.println(server.arg("EN").toInt());
    if (server.arg("FREQ") != 0) {
      lt.channel[ch].frequency = server.arg("FREQ").toInt();
      Serial.print("FREQ: ");
      Serial.println(lt.channel[ch].frequency);
    }
    if (server.arg("NAME") != 0) {
      server.arg("NAME").toCharArray((char*)&lt.channel[ch].name[0], server.arg("NAME").length() + 1);
      Serial.print("NAME: ");
      Serial.println((char*)&lt.channel[ch].name[0]);
    }
    if  (server.arg("RSSI_IN") != 0) {
      lt.channel[ch].trigger_in = server.arg("RSSI_IN").toInt();
      Serial.print("RSSI_IN: ");
      Serial.println(lt.channel[ch].trigger_in);
    }
    if (server.arg("RSSI_OUT") != 0) {
      lt.channel[ch].trigger_out = server.arg("RSSI_OUT").toInt();
      Serial.print("RSSI_OUT: ");
      Serial.println(lt.channel[ch].trigger_out);
    }

    if (ch == 0)
      lt.send = TX_CH1;
    else if (ch == 1)
      lt.send = TX_CH2;
    else if (ch == 2)
      lt.send = TX_CH3;
    else if (ch == 3)
      lt.send = TX_CH4;

  }
  server.send(200, "text/html", page);
}

void handleRoot()
{
  Serial.println("Enter handleRoot");
  String header;
  server.sendHeader("Location", "/laptimer");
  server.sendHeader("Cache-Control", "no-cache");
  server.send(301);
  return;
}

void handleSend()
{
  String t;
  t = server.arg("reset");
  server.send(200);

  if (t == "1") {
    lt.reset = 0x01;
    lt.send = TX_SYS;
  }
  if (t == "2") {
    lt.reset = 0x02;
    lt.send = TX_SYS;
  }
  if (t == "3") {
    lt.reset = 0x04;
    lt.send = TX_SYS;
  }
  if (t == "4") {
    lt.reset = 0x08;
    lt.send = TX_SYS;
  }
}

void handleConfigChannel() {
  server.send(200);
}

/* this callback will be invoked when user request "/sensors" */
void handleSensorData() {
  char txt[1500]; //Usando 1255
  int len = 0;
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;
  float lap_timer;

  sprintf(&txt[len], "{\"hora\": \"%02d\", ", hr);
  len = strlen(txt);
  sprintf(&txt[len], "\"minuto\": \"%02d\", ", min % 60);
  len = strlen(txt);
  sprintf(&txt[len], "\"segundo\": \"%02d\", ", sec % 60);
  len = strlen(txt);

  lap_timer = (float)lt.channel[0].record / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record1\": 0, "); else sprintf(&txt[len], "\"record1\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].record_3 / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record_31\": 0, "); else sprintf(&txt[len], "\"record_31\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].record / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record2\": 0, "); else sprintf(&txt[len], "\"record2\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].record_3 / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record_32\": 0, "); else sprintf(&txt[len], "\"record_32\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].record / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record3\": 0, "); else sprintf(&txt[len], "\"record3\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].record_3 / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record_33\": 0, "); else sprintf(&txt[len], "\"record_33\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].record / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record4\": 0, "); else sprintf(&txt[len], "\"record4\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].record_3 / 1000;
  if (lap_timer == 0) sprintf(&txt[len], "\"record_34\": 0, "); else sprintf(&txt[len], "\"record_34\": %2.3f, ", lap_timer);

  len = strlen(txt);
  if (!lt.channel[0].lap) sprintf(&txt[len], "\"lap1\": \"-\", "); else sprintf(&txt[len], "\"lap1\": %d, ", lt.channel[0].lap);
  len = strlen(txt);
  if (!lt.channel[1].lap) sprintf(&txt[len], "\"lap2\": \"-\", "); else sprintf(&txt[len], "\"lap2\": %d, ", lt.channel[1].lap);
  len = strlen(txt);

  if (!lt.channel[2].lap) sprintf(&txt[len], "\"lap3\": \"-\", "); else sprintf(&txt[len], "\"lap3\": %d, ", lt.channel[2].lap);
  len = strlen(txt);
  if (!lt.channel[3].lap) sprintf(&txt[len], "\"lap4\": \"-\", "); else sprintf(&txt[len], "\"lap4\": %d, ", lt.channel[3].lap);
  len = strlen(txt);

  sprintf(&txt[len], "\"t_min\": %d, ", lt.timer_lap_min / 1000);
  len = strlen(txt);
  sprintf(&txt[len], "\"t_max\": %d, ", lt.timer_lap_max / 1000);
  len = strlen(txt);
  sprintf(&txt[len], "\"t_pass\": %d, ", lt.timer_min_reentry / 1000);
  len = strlen(txt);

  sprintf(&txt[len], "\"en1\": %d, ", lt.channel[CH1].enable);
  len = strlen(txt);
  sprintf(&txt[len], "\"en2\": %d, ", lt.channel[CH2].enable);
  len = strlen(txt);
  sprintf(&txt[len], "\"en3\": %d, ", lt.channel[CH3].enable);
  len = strlen(txt);
  sprintf(&txt[len], "\"en4\": %d, ", lt.channel[CH4].enable);
  len = strlen(txt);
  sprintf(&txt[len], "\"n1\": \"%s\", ", lt.channel[CH1].name);
  len = strlen(txt);
  sprintf(&txt[len], "\"n2\": \"%s\", ", lt.channel[CH2].name);
  len = strlen(txt);
  sprintf(&txt[len], "\"n3\": \"%s\", ", lt.channel[CH3].name);
  len = strlen(txt);
  sprintf(&txt[len], "\"n4\": \"%s\", ", lt.channel[CH4].name);
  len = strlen(txt);
  sprintf(&txt[len], "\"f1\": %d, ", lt.channel[CH1].frequency);
  len = strlen(txt);
  sprintf(&txt[len], "\"f2\": %d, ", lt.channel[CH2].frequency);
  len = strlen(txt);
  sprintf(&txt[len], "\"f3\": %d, ", lt.channel[CH3].frequency);
  len = strlen(txt);
  sprintf(&txt[len], "\"f4\": %d, ", lt.channel[CH4].frequency);
  len = strlen(txt);
  sprintf(&txt[len], "\"in1\": %d, ", lt.channel[CH1].trigger_in);
  len = strlen(txt);
  sprintf(&txt[len], "\"in2\": %d, ", lt.channel[CH2].trigger_in);
  len = strlen(txt);
  sprintf(&txt[len], "\"in3\": %d, ", lt.channel[CH3].trigger_in);
  len = strlen(txt);
  sprintf(&txt[len], "\"in4\": %d, ", lt.channel[CH4].trigger_in);
  len = strlen(txt);
  sprintf(&txt[len], "\"out1\": %d, ", lt.channel[CH1].trigger_out);
  len = strlen(txt);
  sprintf(&txt[len], "\"out2\": %d, ", lt.channel[CH2].trigger_out);
  len = strlen(txt);
  sprintf(&txt[len], "\"out3\": %d, ", lt.channel[CH3].trigger_out);
  len = strlen(txt);
  sprintf(&txt[len], "\"out4\": %d, ", lt.channel[CH4].trigger_out);
  len = strlen(txt);

  lap_timer = (float)lt.channel[0].lap_timer[0] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt11\": \"-\", "); else sprintf(&txt[len], "\"lt11\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[1] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt12\": \"-\", "); else sprintf(&txt[len], "\"lt12\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[2] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt13\": \"-\", "); else sprintf(&txt[len], "\"lt13\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[3] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt14\": \"-\", "); else sprintf(&txt[len], "\"lt14\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[4] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt15\": \"-\", "); else sprintf(&txt[len], "\"lt15\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[5] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt16\": \"-\", "); else sprintf(&txt[len], "\"lt16\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[6] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt17\": \"-\", "); else sprintf(&txt[len], "\"lt17\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[7] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt18\": \"-\", "); else sprintf(&txt[len], "\"lt18\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[8] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt19\": \"-\", "); else sprintf(&txt[len], "\"lt19\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[0].lap_timer[9] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt110\": \"-\", "); else sprintf(&txt[len], "\"lt110\": %2.3f, ", lap_timer);
  len = strlen(txt);

  lap_timer = (float)lt.channel[1].lap_timer[0] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt21\": \"-\", "); else sprintf(&txt[len], "\"lt21\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[1] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt22\": \"-\", "); else sprintf(&txt[len], "\"lt22\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[2] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt23\": \"-\", "); else sprintf(&txt[len], "\"lt23\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[3] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt24\": \"-\", "); else sprintf(&txt[len], "\"lt24\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[4] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt25\": \"-\", "); else sprintf(&txt[len], "\"lt25\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[5] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt26\": \"-\", "); else sprintf(&txt[len], "\"lt26\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[6] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt27\": \"-\", "); else sprintf(&txt[len], "\"lt27\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[7] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt28\": \"-\", "); else sprintf(&txt[len], "\"lt28\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[8] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt29\": \"-\", "); else sprintf(&txt[len], "\"lt29\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[1].lap_timer[9] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt210\": \"-\", "); else sprintf(&txt[len], "\"lt210\": %2.3f, ", lap_timer);
  len = strlen(txt);

  lap_timer = (float)lt.channel[2].lap_timer[0] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt31\": \"-\", "); else sprintf(&txt[len], "\"lt31\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[1] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt32\": \"-\", "); else sprintf(&txt[len], "\"lt32\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[2] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt33\": \"-\", "); else sprintf(&txt[len], "\"lt33\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[3] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt34\": \"-\", "); else sprintf(&txt[len], "\"lt34\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[4] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt35\": \"-\", "); else sprintf(&txt[len], "\"lt35\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[5] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt36\": \"-\", "); else sprintf(&txt[len], "\"lt36\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[6] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt37\": \"-\", "); else sprintf(&txt[len], "\"lt37\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[7] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt38\": \"-\", "); else sprintf(&txt[len], "\"lt38\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[8] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt39\": \"-\", "); else sprintf(&txt[len], "\"lt39\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[2].lap_timer[9] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt310\": \"-\", "); else sprintf(&txt[len], "\"lt310\": %2.3f, ", lap_timer);
  len = strlen(txt);

  lap_timer = (float)lt.channel[3].lap_timer[0] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt41\": \"-\", "); else sprintf(&txt[len], "\"lt41\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[1] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt42\": \"-\", "); else sprintf(&txt[len], "\"lt42\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[2] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt43\": \"-\", "); else sprintf(&txt[len], "\"lt43\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[3] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt44\": \"-\", "); else sprintf(&txt[len], "\"lt44\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[4] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt45\": \"-\", "); else sprintf(&txt[len], "\"lt45\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[5] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt46\": \"-\", "); else sprintf(&txt[len], "\"lt46\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[6] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt47\": \"-\", "); else sprintf(&txt[len], "\"lt47\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[7] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt48\": \"-\", "); else sprintf(&txt[len], "\"lt48\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[8] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt49\": \"-\", "); else sprintf(&txt[len], "\"lt49\": %2.3f, ", lap_timer);
  len = strlen(txt);
  lap_timer = (float)lt.channel[3].lap_timer[9] / 1000;
  if (lap_timer == 0)sprintf(&txt[len], "\"lt410\": \"-\", "); else sprintf(&txt[len], "\"lt410\": %2.3f, ", lap_timer);
  len = strlen(txt);

  sprintf(&txt[len], "\"ad1\": %d, \"ad_max1\": %d, ", lt.channel[0].rssi, lt.channel[0].rssi_max);
  len = strlen(txt);
  sprintf(&txt[len], "\"ad2\": %d, \"ad_max2\": %d, ", lt.channel[1].rssi, lt.channel[1].rssi_max);
  len = strlen(txt);
  sprintf(&txt[len], "\"ad3\": %d, \"ad_max3\": %d, ", lt.channel[2].rssi, lt.channel[2].rssi_max);
  len = strlen(txt);
  sprintf(&txt[len], "\"ad4\": %d, \"ad_max4\": %d}", lt.channel[3].rssi, lt.channel[3].rssi_max);

  server.send(200, "application/json", txt);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}
